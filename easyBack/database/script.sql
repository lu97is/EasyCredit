create table client(
  clientId serial primary key,
  username varchar unique not null ,
  last_update date not null default now(),
  userType char
);

create table records(
  recordId serial,
  quantity numeric not null check (quantity > 0),
  age smallint not null,
  time smallint not null ,
  totalPayment numeric not null ,
  date timestamp not null default now(),
  status char not null,
  client int references client(clientId)
);

CREATE FUNCTION insert_user(q_username character varying, q_usertype char) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO client (clientId, username, last_update, userType) VALUES (default ,q_username, default ,q_usertype);
END
$$;

CREATE FUNCTION get_user(q_username character varying) RETURNS TABLE(t_id int, t_username character varying, t_last_update date, t_userType char )
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
   clientId,
 username,
 last_update,
  userType
 FROM
 client
 WHERE
 username ilike q_username;
END; $$;

CREATE FUNCTION get_record(q_clientId int) RETURNS TABLE(t_quantity numeric, t_age smallint, t_time smallint,
  t_totalPayment numeric, t_date timestamp, t_status char, t_client int )
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
   quantity,
   age,
   time,
   totalPayment,
   date,
   status,
   client
 FROM
 records
 WHERE
 client = q_clientId and status <> 'p';
END; $$;

CREATE FUNCTION get_record_peding(q_clientId int) RETURNS TABLE(t_quantity numeric, t_age smallint, t_time smallint,
  t_totalPayment numeric, t_date timestamp, t_status char, t_client int )
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
   quantity,
   age,
   time,
   totalPayment,
   date,
   status,
   client
 FROM
 records
 WHERE
 client = q_clientId and status = 'p';
END; $$;

CREATE FUNCTION get_record_peding() RETURNS TABLE(t_recordId integer, t_quantity numeric, t_age smallint, t_time smallint,
  t_totalPayment numeric, t_date timestamp, t_status char, t_client character varying )
    LANGUAGE plpgsql
    AS $$
BEGIN
 RETURN QUERY SELECT
   r.recordId,
   r.quantity,
   r.age,
   r.time,
   r.totalPayment,
   r.date,
   r.status,
   c.username
 FROM
 records as r
   inner join client c on r.client = c.clientId
 WHERE
 status = 'p';
END; $$;

CREATE FUNCTION insert_request(q_quantity integer,q_age integer,q_time integer,q_totalPayment numeric, q_client integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO records (recordId,quantity,age,time,totalPayment,date,status,client) VALUES (default ,q_quantity,q_age,q_time,q_totalPayment,default,'p',q_client);
END
$$;

CREATE FUNCTION update_request(q_record_id int, q_response char) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    update records set status = q_response where recordId = q_record_id;
END
$$;

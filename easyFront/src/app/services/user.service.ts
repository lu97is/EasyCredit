import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map, catchError } from "rxjs/operators";
import { of } from 'rxjs';

@Injectable()
export class UserService {
  domain: String = 'http://localhost:8080/';
  user: String;
  password: String;
  constructor(private http: Http) {
  }

  registerUser(user) {
    return this.http.post(this.domain + 'api/user', user).pipe(map(res => res.json())).pipe(catchError(err => of(`Error ${err}`)));
  }

  getUser(user) {
    return this.http.get(this.domain + 'api/user/' + user).pipe(map(res => res.json()));
  }

  getRecords(user) {
    return this.http.get(this.domain + 'api/records/' + user).pipe(map(res => res.json()));

  }

  getRecordsPending(user) {
    return this.http.get(this.domain + 'api/recordsPending/' + user).pipe(map(res => res.json()));
  }

  getAllRecordsPending() {
    return this.http.get(this.domain + 'api/recordsPending').pipe(map(res => res.json()));
  }

  postRecord(record) {
    return this.http.post(this.domain + 'api/records', record).pipe(map(res => res.json())).pipe(catchError(err => of(`Error ${err}`)));
  }

  putRecord(id, response) {
    return this.http.put(this.domain + 'api/records_update/' + id, response).pipe(map(res => res.json())).pipe(catchError(err => of(`Error ${err}`)));
  }



}

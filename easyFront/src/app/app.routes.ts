import { RouterModule, Routes } from '@angular/router';
import {LandingComponent} from './components/landing/landing.component'
import {DashboardComponent} from './components/dashboard/dashboard.component'
import {AdminComponent} from './components/admin/admin.component'
const app_routes: Routes = [
  { path: 'home', component: LandingComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'admin', component: AdminComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' },

];

export const app_routing = RouterModule.forRoot(app_routes);
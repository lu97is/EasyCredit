import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {UserService} from '../../services/user.service'
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';

import { LandingComponent } from './landing.component';

describe('LandingComponent', () => {
  
  let component: LandingComponent;
  let fixture: ComponentFixture<LandingComponent>;
  
  
 

 beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LandingComponent],
       imports :[ RouterTestingModule, ReactiveFormsModule, FormsModule,HttpModule ],
       providers: [UserService]
    })
   
  }));
  beforeEach(()=>{
    fixture = TestBed.createComponent(LandingComponent);
    component = fixture.componentInstance;
    component.createForm();
   });
  
  it('should be created',inject([UserService], () => {
    const fixture = TestBed.createComponent(LandingComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  
  it('form invalid when empty',  () => {
    expect(component.form.valid).toBeFalsy();
  });
  it('username field empty', () => {
    let username = component.form.controls['username']; 
    expect(username.valid).toBeFalsy(); 
  });

  it('username field validity', () => {
    let errors = {};
    let username = component.form.controls['username'];
    errors = username.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });


});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service'
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  form: FormGroup;
  failedRegister: Boolean = false;
  failedLogin: Boolean = false;

  // Se inicializan formBuilder,Router y el servicio UserService.
  constructor(private fb: FormBuilder, private router: Router, private _user: UserService) {
    // es llamada esta funcion para poder usar el formulario correspondiente
    this.createForm();
  }

  ngOnInit() {
  }

  // Funcion para crear el formulario que sera utilizado para acceder
  createForm() {
    this.form = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]]
    });
  }
  // Funcion para registrar un usuario nuevo, la cual obtiene los datos de:
  // Nombre de usuario y tipo de usuario, posteriormente se llama el servicio
  // _user el cual contiene una funcion para hacer una llamada al servidor la cual 
  // registra al usuario.
  // Posteriormente es suscrita a la respuesta y damos feedback al usuario.
  register() {
    const userCredentials = {
      username: this.form.get('username').value,
      usertype: 'u'
    }

    this._user.registerUser(userCredentials).subscribe(data => {
      if (data.success) {
        localStorage.setItem('user', userCredentials.username);
        this.router.navigate(['/dashboard']);
      } else {
        this.failedRegister = true;
      }
    });

  }
  // Funcion para acceder a un usuario registrado, la cual obtiene los datos de:
  // Nombre de usuario, posteriormente se llama el servicio
  // _user el cual contiene una funcion para hacer una llamada al servidor la cual
  // accede al usuario.
  // Posteriormente es suscrita a la respuesta y damos feedback al usuario.
  login() {
    this._user.getUser(this.form.get('username').value).subscribe(data => {
      if (!data.message[0]) {
        this.failedLogin = true;
      } else {
        localStorage.setItem('user', data.message[0].t_username);
        this.router.navigate(['/dashboard']);
      }
    })
  }

}

import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {UserService} from '../../services/user.service'
import { NavbarComponent } from "../navbar/navbar.component";
import { HttpModule } from '@angular/http';

import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  
  
 

 beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent, NavbarComponent],
       imports :[ RouterTestingModule, ReactiveFormsModule, FormsModule,HttpModule ],
       providers: [UserService]
    })
   
  }));
  beforeEach(()=>{
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    component.createForm();
   });
  
  it('should be created',inject([UserService], () => {
    const fixture = TestBed.createComponent(DashboardComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  
  it('form invalid when empty',  () => {
    expect(component.form.valid).toBeFalsy();
  });


  it('quantity field empty', () => {
    let quantity = component.form.controls['quantity']; 
    expect(quantity.valid).toBeFalsy(); 
  });

  it('quantity field validity', () => {
    let errors = {};
    let quantity = component.form.controls['quantity'];
    errors = quantity.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });

  it('age field empty', () => {
    let age = component.form.controls['age']; 
    expect(age.valid).toBeFalsy(); 
  });

  it('age field validity', () => {
    let errors = {};
    let age = component.form.controls['age'];
    errors = age.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });

  it('creditCar field empty', () => {
    let creditCar = component.form.controls['creditCar']; 
    expect(creditCar.valid).toBeFalsy(); 
  });

  it('creditCar field validity', () => {
    let errors = {};
    let creditCar = component.form.controls['creditCar'];
    errors = creditCar.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });

  it('time field empty', () => {
    let time = component.form.controls['time']; 
    expect(time.valid).toBeFalsy(); 
  });

  it('time field validity', () => {
    let errors = {};
    let time = component.form.controls['time'];
    errors = time.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });


  // quantity: ['', [Validators.required, Validators.min(1)]],
  //     age: ['', [Validators.required, Validators.min(20), Validators.max(80)]],
  //     creditCar: [false, Validators.required],
  //     time: ['', Validators.required]


});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service'


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  form: FormGroup;
  totalPayment: any;
  username: String;
  clientId: any;
  loading: Boolean = false;
  records: any[] = [];
  recordsPending: any[] = [];
  response: String;
  aproved: number = 0;

  // Se realizan 3 llamadas al servicio
  // 1-Obtenemos el nombre de usuario guardado en el local storage para hacer la
  //    llamada al servidor la cual nos regresa la informacion del usuario enviado
  // 2-Se obtienen las solicitudes pendientes del usaurio
  // 3-Se obtiene el historial del usuario
  constructor(private fb: FormBuilder, private _user: UserService) {
    _user.getUser(localStorage.getItem('user')).subscribe(data => {
      this.username = data.message[0].t_username;
      this.clientId = data.message[0].t_id;
      _user.getRecords(data.message[0].t_id).subscribe(data => {
        this.records = data.message;
        data.message.map(r => {
          // Aqui solamente se suman las que fueron aprobadas para realizar un 
          // calculo posteriormente
          if (r.t_status == 'a') {
            this.aproved++;
          }
        })
      })
      _user.getRecordsPending(data.message[0].t_id).subscribe(data => {
        this.recordsPending = data.message;
      })
    })
    // Se crea el formualario de solicitud de prestamo
    this.createForm();
    // Se crea una funcion para estar escuchando cuando el formulario 
    // tenga cambios.
    this.onChanges();
  }

  ngOnInit() {


  }

  createForm() {
    this.form = this.fb.group({
      quantity: ['', [Validators.required, Validators.min(1)]],
      age: ['', [Validators.required, Validators.min(20), Validators.max(80)]],
      creditCar: ['', Validators.required],
      time: ['', Validators.required]
    });
  }
  // Funcion para enviar la solicitud al servidor
  save() {
    let months;
    // Se obtiene la cantidad de meses que el usuario eligio para pagar el prestamo
    switch (this.form.get('time').value) {
      case 1.05:
        months = 3;
      case 1.07:
        months = 6;
      case 1.12:
        months = 12;
      default:
        months = 3;
    }
    // Se guardan todos los valores del formulario en una constante
    const request = {
      quantity: this.form.get('quantity').value,
      age: this.form.get('age').value,
      time: months,
      totalPayment: this.totalPayment,
      client: this.clientId
    }
    this.loading = true;
    // Se hace una llamada al servicio _user para guardar la solicitud que esta siendo
    // enviada
    this._user.postRecord(request).subscribe(data => {
      if (data.success) {
        this.loading = false;
        this.response = data.message;
      } else {
        this.loading = false;
        this.response = data.message;
      }
    })
  }

  // Funcion para obtener las solicitudes del usuario, tanto las pendientes
  // como las que ya fueron aprobadas
  getRecords() {
    this._user.getRecords(this.clientId).subscribe(data => {
      this.records = data.message;
    })
    this._user.getRecordsPending(this.clientId).subscribe(data => {
      this.recordsPending = data.message;
    })
  }
  // Funcion que se llama cuando se hace un cambio al formulario para calcular el pago
  // final despues del plazo
  onChanges(): void {
    this.form.valueChanges.subscribe(val => {
      this.totalPayment = parseFloat((this.form.get('quantity').value * this.form.get('time').value).toFixed(2));
    })
  }

}

import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {UserService} from '../../services/user.service'
import { HttpModule } from '@angular/http';

import { AdminComponent } from './admin.component';
import { NavbarComponent } from "../navbar/navbar.component";

describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminComponent, NavbarComponent ],
      imports :[ RouterTestingModule, ReactiveFormsModule, FormsModule, HttpModule],
      providers: [UserService]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', inject([UserService], () => {
    expect(component).toBeTruthy();
  }));
});

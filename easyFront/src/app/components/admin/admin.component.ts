import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent implements OnInit {
  requests:any [] = [];
  loading: boolean = false;
  user:String;
  responseText:String;
  admin: boolean;
  logged: boolean = false;
  // Se inicializa el servicio UserService
  constructor(private _user:UserService) { }

  ngOnInit() {
  }
  // Funcion para dar acceso al administrador la cual verifica que exista y 
  // su rol sea de tipo administrador.
  login(){
    this._user.getUser(this.user).subscribe(data => {
      if(data.message[0]){
        if(data.message[0].t_usertype == 'a'){
        this.getRecords();
        this.admin = true;
        this.logged = true;
        }
      }else{
        this.admin = false;
        this.responseText = 'Parece que no eres administrador.'
      }
    })
    
  }
  // Funcion la cual obtiene todas la solicitudes pendientes hechas por los usuarios
  getRecords(){
    this._user.getAllRecordsPending().subscribe(data => {
      this.requests = data.message;
    })
  }
  // Funcion para procesar la solicitud de un usuario 
  // mediante una llamada al servicio
  // _user el cual contiene una funcion para hacer una llamada al servidor la cual
  // actualiza los registros del usuario.
  // Posteriormente es suscrita a la respuesta y damos feedback al usuario.
  process(res,i){
    this.loading = true;
    let body = {
      response : res
    }
    this._user.putRecord(i,body).subscribe(data => {
      this.loading = false;
      if(data.success){
        this.getRecords();
      }else{
        alert('error')
      }
    })
  }
}

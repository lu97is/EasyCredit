import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  // Se libera del local storage el usuario que habia accedido y se envia de 
  // vuelta al home.

  logOut() {
    localStorage.removeItem('user');
    this.router.navigate(['/home']);
  }

}

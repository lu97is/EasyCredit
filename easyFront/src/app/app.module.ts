import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {app_routing} from './app.routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpModule } from '@angular/http';

import {UserService} from './services/user.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AdminComponent } from './components/admin/admin.component'


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    DashboardComponent,
    NavbarComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [
    UserService
  ],
 
  bootstrap: [AppComponent]
})
export class AppModule { }

# EasyCredit

Easycredit es una plataforma para solicitudes de creditos en base a ciertos parametros.

  - El usuario debe ser mayor a 20 años
  - El usuario debe tener tarjeta de credito

El programa puede ser probado en la siguiente url:
> https://easycreditexam.herokuapp.com/


Y el video en la siguiente url:

> https://www.useloom.com/share/409909e137904b92864f7197aeaec95f


### Prerequisites

Easycredit necesita nodeJS en su version 8.11.4^ (version actual) y Npm en su version 6.1.0 ^ (version actual).
Para verificar la version de node es con el siguiente comando
```sh
$ node -v
```

Y para npm es con el siguiente
```sh
$ npm -v
```

A su vez no es necesario tener Angular instalado para mirar la funcionalidad del programa
pero si sera necesario si es necesario realizar cambios al codigo.
Es por ello que se necesita Angular en su version 6.

Para verificar la version de angular es con el siguiente comando
```sh
$ ng version
```

Tanto como Node, Npm y Angular se pueden descargar de su pagina oficial.

Para node es esta
> https://nodejs.org/es/

Para npm es esta
> https://www.npmjs.com/

y para angular
> https://angular.io/

Tambien sera necesaria la ejecución del script para generar la base datos necesaria.

```sh
$ cd easyBack
$ cd database
```
En esa carpeta se encuentra el script el cual debera ser ejecutado en algun sistema
gestor de base de datos, en este caso es recomendado postgreSQL, ya que ahi fue donde se realizo la base de datos.


### Installation
Para clonar el repositorio es con el siguiente comando.
 ```sh
  $ git clone https://gitlab.com/lu97is/EasyCredit.git
 ```

Posterior instalaremos las dependencias necesarias para su ejecución.
```sh
$ cd EasyCredit
$ cd easyBack
$ npm install
```
Y tambien 
```sh
$ cd EasyCredit
$ cd easyFront
$ npm install
```
Esto puede tomar unos cuantos segundos.

A su vez hay dos comandos para arrancar el servidor.
Para iniciar el servidor

```sh
$ cd easyBack
$ npm start
```
Para iniciar el servidor y que este este pendiente de los cambios realizados.

```sh
$cd easyBack
$ npm run startMon
```
Posteriormente abrir el navegador con la siguiente url
```sh
http://localhost:8080
```

### Ejecutar pruebas
Para ejecutar las pruebas sera necesario lo siguiente
```sh
$ cd easyFront
$ ng test
```


### Tech

En este proyecto se encunetran diferentes tecnologias empleadas:

* [Angular]
* [NodeJs] 
* [Express] 
* [PostgreSQL] 
* [pg-promise] 
* [Bluebird] 
* [Heroku]
* [Jasmine]
* [Karma]

### Comentarios Extra
> El usuario para entrar como administrador el easyCredit o easyCreditAdmin

El programa ya esta compilado para produccion, lo que quiere decir que se encuentra optimizado.
Pero aun asi si desea correr el programa de manera de desarrollo, se puede realizar de la siguiente manera
```sh
$ cd easyFront
$ ng serve
```
Posteriormente dirigirse a la url:
```
http://localhost:4200
```
Al finalizar los cambios que sean realizados, ejecutar el siguiente comando para que se genere un archivo de produccion, no se preocupe, ya esta configurado para que solamente genere ese archivo y el backend lo tome automaticamente.
```sh
$ cd easyFront
$ ng build --prod
```

Se tendra que configurar la conexion a la base de datos en caso de querer utilizar la propia
```sh
$ cd easyBack
$ cd routes
```
En el archivo 'api.js' se tendra que modificar esta linea de codigo
```javascript
 const connectionString = 'postgres://ikblbuhpnkjfoq:11df6e723607cde9579686930b5b87f832b63b699a9a2b711f91356db6df5609@ec2-54-235-160-57.compute-1.amazonaws.com:5432/ddvd9q1jhp1lvb'
``` 
 Que seria de la siguiente manera
```javascript

    const connectionString = 'postgres://<username><password>@<host>:<puerto>/<nombrebasedatos>
```

Un ejemplo de una conexion local seria la siguiente
 
```javascript
const connectionString = 'postgres://postgres:mypassword123@localhost:5432/easycredit'
```

Y en el caso del front se tendra que cambiar el dominio por uno propio.
```sh
$ cd easyFront
$ cd src
$ cd app
$ cd services
```
Y el archivo 'user.service.ts' en la linea de codigo 
```javascript
domain: String = 'https://easycreditexam.herokuapp.com/';
```

se tendra que cambiar, un ejemplo de un dominio local es el siguiente
```javascript
domain: String = 'http://localhost:8080/';
```

### Problemas 
- Existieron problemas al momento de realizar las pruebas unitarias ya que era algo nuevo para mi.
- Debido a las capacidades de mi computadora, la grabacion de la pantalla fue un tanto complicado(estoy trabajando en arregalara)

### Critica

- Un examen muy completo y diferente a lo que he visto que realizan las empresas evaluan para que entren a ella. 
- Tambien fue un tanto divertida y entretenida para mi. 
- Me parecio algo curioso que no fuera necesaria la parte de la seguridad, pero entiendo que lo importante es la funcionalidad. 
- Me queda muy claro y creo que a ustedes tambien, que necesito alguna clase de mentoria en la cuestion de los diseños y claramente la eleccion de los colores.
- Me encanto realizar este examen y la verdad espero sea lo que estan buscando y claro, tambien les guste lo que realize

Por mi parte es todo y espero poder ser parte de su equipo ya que en el poco tiempo que he estado dentro de concentro, sin ser parte de ustedes, era increible como desde que entras ya eres tratado como parte de esa gran familia que estan construyendo.


